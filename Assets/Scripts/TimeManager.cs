﻿using UnityEngine;

namespace Assets.Scripts
{
    public class TimeManager : MonoBehaviour
    {

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (IsStarted)
            {
                _currentTime += Time.deltaTime;
            }
        }

        public bool IsStarted = false;

        public float TotalTime
        {
            get { return _currentTime; }
        }

        private float _currentTime = 0;
    }
}
