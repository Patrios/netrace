﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(NetworkView))]
    public class WinnerController : MonoBehaviour
    {
        public void Awake()
        {
            WinnerName = string.Empty;
        }

        public void RegistrateWinner(string player)
        {
            networkView.RPC("NetworkRegistrateWinne", RPCMode.All, player);
        }

        [RPC]
        private void NetworkRegistrateWinne(string player)
        {
            if (WinnerName != string.Empty)
            {
                WinnerName = player;
            }
        }

        public static string WinnerName { get; private set; }
    }
}
