﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class GameData
    {
        public static string ClientPlayerName = "";
        public static string ServerPlayerName = "";
        public static string ServerIP = "";
        public static float FinishTime;

        public static string OpponentName
        {
            get { return Network.isServer ? ClientPlayerName : ServerPlayerName; }
        }

        public static string PlayerName { get; set; }
    }
}
