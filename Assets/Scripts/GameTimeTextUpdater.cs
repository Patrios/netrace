﻿using System;
using System.Globalization;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameTimeTextUpdater : MonoBehaviour
    {
        // Update is called once per frame
        private void FixedUpdate()
        {
            var raceTime = Math.Round(Utils.GetTimeInRace(), 1);
            GetComponent<GUIText>().text = raceTime.ToString(CultureInfo.InvariantCulture);
        }
    }
}