﻿using UnityEngine;

namespace Assets.Scripts
{
    [ExecuteInEditMode]
    public class DisplayLogo : MonoBehaviour
    {
        private void OnGUI()
        {
            if (LogoTexture != null)
            {
                GUI.DrawTexture(LogoRect, LogoTexture, ScaleMode.StretchToFill, true);
            }
        }

        private Rect LogoRect
        {
            get
            {
                return new Rect(0, 0, Screen.width, Screen.height);
            }
        }

        public Texture LogoTexture;
    }
}