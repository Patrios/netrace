﻿using Assets.Scripts.Web;
using UnityEngine;

namespace Assets.Scripts
{
    [ExecuteInEditMode]
    public sealed class MainMenu : MonoBehaviour
    {
        public Vector2 ButtonExit;
        public Vector2 ButtonStartGame;
        public Vector2 ButtonNetwork;
        public Vector2 BackToMainMenu;
        public Vector2 StartServer;
        public Vector2 ConnectToServer;
        public Vector2 ServerIp;
        public Vector2 WaitText;
        public Vector2 YouConnectedText;
        public Vector2 StartP2PGame;
        public Vector2 LanErrorText;

        public Vector2 PlayerNameLable;
        public Vector2 PasswordNameLable;
        public Vector2 PlayerNameField;
        public Vector2 PasswordPasswordField;
        public Vector2 FieldSize;
        public Vector2 AuthorizeButton;
        public GUIStyle AuthorizationTextStyle;
        public Vector2 AuthorizationProblem;
        public GUIStyle ProblemStyle;


        private void Start()
        {
            Network.minimumAllocatableViewIDs = 500;
            _warningTextStyle = new GUIStyle
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 20
                };

            _currentState = string.IsNullOrEmpty(GameData.PlayerName) ? MainMenuStates.Authorization : MainMenuStates.MainMenu;
        }

        private void OnGUI()
        {
            switch (_currentState)
            {
                case MainMenuStates.Authorization:
                    AuthorizationHandle();
                    break;

                case MainMenuStates.MainMenu:
                    MainMenuHandle();
                    break;

                case MainMenuStates.NetwokMenu:
                    NetworkMenuHandle();
                    break;

                case MainMenuStates.WaitPlayerToConnect:
                    WaitPlayerToConnectHandle();
                    break;

                case MainMenuStates.ConnectedWithPlayer:
                    ConnectedWithPlayerHandle();
                    break;

                case MainMenuStates.WaitToStartGame:
                    WaitToStartGameHandle();
                    break;

                case MainMenuStates.ConnectingToServer:
                    ConnectingToServerHandle();
                    break;
            }
        }

        [RPC]
        private void InitializeOpponentName(string opponentName)
        {
            if (Network.isServer)
            {
                GameData.ServerPlayerName = GameData.PlayerName;
                GameData.ClientPlayerName = opponentName;
            }
            else
            {
                GameData.ServerPlayerName = opponentName;
                GameData.ClientPlayerName = GameData.PlayerName;
            }
        }

        [RPC]
        private void StartGame()
        {
            Application.LoadLevel(Consts.RaceLevelName);
        }

        private void OnPlayerConnected(NetworkPlayer player)
        {
            Debug.Log("Player connected.");
            _lasterError = NetworkConnectionError.NoError;
            _currentState = MainMenuStates.ConnectedWithPlayer;
            networkView.RPC("InitializeOpponentName", RPCMode.Others, GameData.PlayerName);
        }

        private void OnFailedToConnect(NetworkConnectionError error)
        {
            Debug.Log("Connection faild to server.");
            _lasterError = error;
            _currentState = MainMenuStates.NetwokMenu;
        }

        private void OnConnectedToServer()
        {
            GameData.ServerIP = _lanGameIp;
            Debug.Log("Connected to server.");
            _currentState = MainMenuStates.WaitToStartGame;
            networkView.RPC("InitializeOpponentName", RPCMode.Others, GameData.PlayerName);
        }

        private void OnPlayerDisconnected(NetworkPlayer player)
        {
            Debug.Log("Player disconnected.");
            _lasterError = NetworkConnectionError.ConnectionFailed;
            _currentState = MainMenuStates.NetwokMenu;
        }

        private void OnDisconnectedFromServer()
        {
            Debug.Log("Server disconnected.");
            _lasterError = NetworkConnectionError.ConnectionFailed;
            _currentState = MainMenuStates.NetwokMenu;
        }

        #region MenuHandlers

        private string _playerName = "";
        private string _password = "";
        private bool _isAuthorizationProblem = false;

        private void AuthorizationHandle()
        {
            if (_isAuthorizationProblem)
            {
                GUI.Label(
                    Utils.ButtonRect(AuthorizationProblem), 
                    "There is no such user or incorrect password!", 
                    ProblemStyle);
            }

            GUI.Label(
                Utils.ButtonRect(PlayerNameLable), 
                "Player name:",
                AuthorizationTextStyle);

            GUI.Label(
                Utils.ButtonRect(PasswordNameLable),
                "Passord:",
                AuthorizationTextStyle);

            _playerName = GUI.TextField(
                Utils.CenterdRect(PlayerNameField, (int)FieldSize.x, (int)FieldSize.y),
                _playerName);

            _password = GUI.PasswordField(
                Utils.CenterdRect(PasswordPasswordField, (int)FieldSize.x, (int)FieldSize.y),
                _password,
                '*');

            if (GUI.Button(Utils.ButtonRect(AuthorizeButton), "Authorize"))
            {
                Debug.Log(string.Format(
                    "Try to Authorize with login: {0} and password: {1}",
                    _playerName,
                    _password));

                if (WebServiceUtils.GetUserService().IsRegistrated(_playerName, _password))
                {
                    SaveLocalPlayerData();
                    _currentState = MainMenuStates.MainMenu;
                }
                else
                {
                    _isAuthorizationProblem = true;
                }
            }
        }

        private void SaveLocalPlayerData()
        {
            GameData.PlayerName = _playerName;
        }

        private void ConnectingToServerHandle()
        {
            GUI.Label(Utils.ButtonRect(YouConnectedText), "Connecting...", _warningTextStyle);

            if (GUI.Button(Utils.ButtonRect(BackToMainMenu), "Back."))
            {
                Debug.Log("You press Back to main menu button!");

                Network.Disconnect();

                _currentState = MainMenuStates.NetwokMenu;
            }
        }

        private void WaitToStartGameHandle()
        {
            GUI.Label(
                Utils.ButtonRect(YouConnectedText),
                "You connected with player: " + GameData.OpponentName, 
                _warningTextStyle);


            if (GUI.Button(Utils.ButtonRect(BackToMainMenu), "Back."))
            {
                Debug.Log("You press Back to main menu button!");

                Network.Disconnect();

                _currentState = MainMenuStates.NetwokMenu;
            }
        }

        private void ConnectedWithPlayerHandle()
        {
            GUI.Label(
                Utils.ButtonRect(YouConnectedText),
                "You connected with player: " + GameData.OpponentName, 
                _warningTextStyle);

            if (GUI.Button(Utils.ButtonRect(StartP2PGame), "Start game!"))
            {
                Debug.Log("You Start game with player");
                networkView.RPC("StartGame", RPCMode.All);
            }

            if (GUI.Button(Utils.ButtonRect(BackToMainMenu), "Back."))
            {
                Debug.Log("You press Back to main menu button!");
                Network.Disconnect();
                _currentState = MainMenuStates.NetwokMenu;
            }
        }

        private void WaitPlayerToConnectHandle()
        {
            GUI.Label(Utils.ButtonRect(WaitText), "Wait Players...", _warningTextStyle);

            if (GUI.Button(Utils.ButtonRect(BackToMainMenu), "Back."))
            { 
                Debug.Log("You press Back to main menu button!");

                Network.Disconnect();

                _currentState = MainMenuStates.NetwokMenu;
            }
        }

        private void NetworkMenuHandle()
        {
            _lanGameIp = GUI.TextField(Utils.CenterdRect(ServerIp, 70, 20), _lanGameIp, 15);

            if (GUI.Button(Utils.ButtonRect(BackToMainMenu), "Back."))
            {
                Debug.Log("You press Back to main menu button!");

                _currentState = MainMenuStates.MainMenu;
            }

            if (GUI.Button(Utils.ButtonRect(StartServer), "Start server."))
            {
                Debug.Log("You press Start server button!");

                Network.InitializeServer(Consts.ServerConnections, Consts.ServerPort, false);

                _currentState = MainMenuStates.WaitPlayerToConnect;
            }

            if (GUI.Button(Utils.ButtonRect(ConnectToServer), "Connect to server."))
            {
                Debug.Log("You press Connect to server button!");

                var errors = Network.Connect(_lanGameIp, Consts.ServerPort);
                Debug.Log(errors);


                _lasterError = errors;
                if (errors == NetworkConnectionError.NoError)
                {
                    _currentState = MainMenuStates.ConnectingToServer;
                }
            }

            if (_lasterError != NetworkConnectionError.NoError)
            {
                GUI.Label(Utils.ButtonRect(LanErrorText), "Lan Error: " + _lasterError, _warningTextStyle);
            }
        }

        private void MainMenuHandle()
        {
            if (GUI.Button(Utils.ButtonRect(ButtonExit), "Exit."))
            {
                Debug.Log("You press Exit Game button!");
                Application.Quit();
            }

            if (GUI.Button(Utils.ButtonRect(ButtonStartGame), "Find opponent for me!"))
            {
                Debug.Log("You press Start button!");

                var opponent = WebServiceUtils.GetUserService().GetOpponent(GameData.PlayerName, Network.player.ipAddress);

                Debug.Log(string.Format("Am i Server: {0}", opponent.IP));

                if (opponent.IsServer)
                {
                    Network.Connect(opponent.IP, Consts.ServerPort);
                    _currentState = MainMenuStates.ConnectingToServer;
                }
                else
                {
                    Network.InitializeServer(Consts.ServerConnections, Consts.ServerPort, false);
                    WebServiceUtils.GetUserService().SetServerStarted(GameData.PlayerName);
                    _currentState = MainMenuStates.WaitPlayerToConnect;
                }
            }

            if (GUI.Button(Utils.ButtonRect(ButtonNetwork), "Network game."))
            {
                _currentState = MainMenuStates.NetwokMenu;
                Debug.Log("You press Network game!");
            }
        }

        #endregion

        private MainMenuStates _currentState = MainMenuStates.Authorization;
        private GUIStyle _warningTextStyle;

        private enum MainMenuStates
        {
            Authorization,
            MainMenu,
            NetwokMenu,
            WaitPlayerToConnect,
            ConnectedWithPlayer,
            WaitToStartGame,
            ConnectingToServer
        }

        private NetworkConnectionError _lasterError = NetworkConnectionError.NoError;
        private string _lanGameIp = "";
    }
}
