﻿using System.Globalization;
using UnityEngine;

namespace Assets.Scripts
{
    public class CountDownManager : MonoBehaviour
    {
        private void Awake()
        {
            networkView.stateSynchronization = NetworkStateSynchronization.ReliableDeltaCompressed;
        }

        // Use this for initialization
        private void Start()
        {
            OnGameControlScript.GameInActiveState = false;
            _currentCount = MaxCount;
            _prevTime = Time.time;
            SetCountDownText(_currentCount.ToString(CultureInfo.InvariantCulture));
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (Network.isServer)
            {
                if (Time.time - _prevTime > CountDownTime)
                {
                    networkView.RPC("CountTick", RPCMode.All);
                }
            }
        }

        [RPC]
        private void CountTick()
        {
            _currentCount--;
            _prevTime = Time.time;

            if (_currentCount == -1)
            {
                Debug.Log("Destroy!!");
                Destroy(gameObject);
            }


            if (_currentCount == 0)
            {
                OnGameControlScript.GameInActiveState = true;
                SetCountDownText("GO!");
                Utils.StartTimeManager();
                //Instantiate(TimeDisplayPrefab);
                Debug.Log("Start!");
            }
            else
            {
                SetCountDownText(_currentCount.ToString(CultureInfo.InvariantCulture));
            }
        }

        public float CountDownTime = 1.0f;
        public int MaxCount = 3;
        public GameObject TimeDisplayPrefab;

        private void SetCountDownText(string text)
        {
            guiText.text = text;
        }

        private int _currentCount = 0;
        private float _prevTime;
    }
}