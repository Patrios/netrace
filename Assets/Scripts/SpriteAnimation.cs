﻿using UnityEngine;

// ReSharper disable once UnusedMember.Global
namespace Assets.Scripts
{
    public sealed class SpriteAnimation : MonoBehaviour
    {

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {
            HandleAnimation();
        }

        public int CurrentAnim = 0;
        public int Columns = 8;
        public int Rows = 8;
        public int CurrentFrame = 1;
        public float AnimTime = 0.0f;
        public float fps = 10.0f;
        public bool Explode = false;

        private void HandleAnimation()
        {
            FindAnimation();
            PlayAnimation();
        }

        void FindAnimation()
        {
            var playerMovement = transform.parent.GetComponent<PlayerMovement>();
            var currentHoriz = Input.GetAxis("Horizontal");
            _carVelocity = playerMovement.CurrentSpeed;

            if (_carVelocity > 0.1f)
            {
                CurrentAnim = AnimDrive;
                if (currentHoriz < 0.0f)
                {
                    CurrentAnim = AnimDriveLeft;
                }
                if (currentHoriz > 0.0f)
                {
                    CurrentAnim = AnimDriveRight;
                }
            }
            if (_carVelocity < 0.1f)
            {
                CurrentAnim = AnimIdle;
            }

            if (Explode)
            {
                CurrentAnim = AnimExplosion;
            }
        }

        private void PlayAnimation()
        {
            AnimTime -= Time.deltaTime;
            if (AnimTime <= 0)
            {
                CurrentFrame += 1;
                AnimTime += 1.0f / fps;
            }

            if (CurrentAnim == AnimExplosion)
            {
                CurrentFrame = Mathf.Clamp(CurrentFrame, ExplosionMin, ExplosionMax + 1);
                if (CurrentFrame > ExplosionMax) //Does not work if currentAnim == animIdle! BUG
                {
                    Explode = false;
                }
            }

            if (CurrentAnim == AnimIdle)
            {
                CurrentFrame = Mathf.Clamp(CurrentFrame, Idle, Idle);
            }

            if (CurrentAnim == AnimDrive)
            {
                CurrentFrame = Mathf.Clamp(CurrentFrame, DriveMin, DriveMax + 1);
                if (CurrentFrame > DriveMax)
                {
                    CurrentFrame = DriveMin;
                }
            }
            if (CurrentAnim == AnimDriveLeft)
            {
                CurrentFrame = Mathf.Clamp(CurrentFrame, DriveLeftMin, DriveLeftMax + 1);
                if (CurrentFrame > DriveLeftMax)
                {
                    CurrentFrame = DriveLeftMin;
                }
            }
            if (CurrentAnim == AnimDriveRight)
            {
                CurrentFrame = Mathf.Clamp(CurrentFrame, DriveRightMin, DriveRightMax + 1);
                if (CurrentFrame > DriveRightMax)
                {
                    CurrentFrame = DriveRightMin;
                }
            }

            _framePosition.y = 1;

            var i = 0;
            for (i = CurrentFrame; i > Columns; i -= Rows)
            {
                _framePosition.y += 1;
            }

            _framePosition.x = i - 1;

            _frameSize = new Vector2(1.0f / Columns, 1.0f / Rows);
            _frameOffset = new Vector2(_framePosition.x / Columns, 1.0f - (_framePosition.y / Rows));

            renderer.material.SetTextureScale("_MainTex", _frameSize);
            renderer.material.SetTextureOffset("_MainTex", _frameOffset);	
        }

        private Vector2 _framePosition;
        private Vector2 _frameSize;
        private Vector2 _frameOffset;

        private const int Idle = 17;
        private int _idleLeft = 1;
        private int _idleRight = 2;
        private const int DriveMin = 3;
        private const int DriveMax = 4;
        private const int DriveLeftMin = 5;
        private const int DriveLeftMax = 6;
        private const int DriveRightMin = 7;
        private const int DriveRightMax = 8;
        private int _spin = 9;
        private const int ExplosionMin = 10;
        private const int ExplosionMax = 16;

        private const int AnimIdle = 0;
        private int _animIdleLeft = 1;
        private int _animIdleRight = 2;
        private const int AnimDrive = 3;
        private const int AnimDriveLeft = 4;
        private const int AnimDriveRight = 5;
        private int _animSpin = 6;
        private const int AnimExplosion = 7;
        private float _carVelocity;
    }
}
