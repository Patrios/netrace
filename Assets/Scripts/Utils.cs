﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace Assets.Scripts
{
    public static class Utils
    {
        public static Rect ButtonRect(Vector2 buttonVect)
        {
            return CenterdRect(buttonVect, 280, 80);
        }

        public static Rect CenterdRect(Vector2 buttonVect, int width, int height)
        {
            return new Rect(
                Consts.ScreenCenter.x + buttonVect.x,
                Consts.ScreenCenter.y + buttonVect.y,
                width,
                height);
        }

        public static void StartTimeManager()
        {
            var timeManager = GameObject.FindGameObjectWithTag(Consts.TimeManagerTag);
            timeManager.GetComponent<TimeManager>().IsStarted = true;
        }

        public static void StopTimeManager()
        {
            var timeManager = GameObject.FindGameObjectWithTag(Consts.TimeManagerTag);
            timeManager.GetComponent<TimeManager>().IsStarted = false;
        }

        public static float GetTimeInRace()
        {
            var timeManager = GameObject.FindGameObjectWithTag(Consts.TimeManagerTag);

            return timeManager.GetComponent<TimeManager>().TotalTime;
        }

        public static int GetTotalScope()
        {
            return (int)(Consts.MaxScope/(1.0f + GameData.FinishTime)) + 
                (IsCurrentPlayerWinner() ? Consts.WinnerScope : 0);
        }

        public static string WinnerName()
        {
            var crossObject = GameObject.Find("PFB_FinishCheckPoint");
            return crossObject.GetComponent<FinishCross>().WinnerName;
        }

        public static bool IsCurrentPlayerWinner()
        {
            return WinnerName() == GameData.PlayerName;
        }

        public static string GetVkPostMessage()
        {
            return IsCurrentPlayerWinner() ? 
                string.Format("Yes! I win Race in Net Races! My scope is: " + GetTotalScope()) : 
                string.Format("Oh, no =( I'm loose... but me scope is: " + GetTotalScope());
        }

        public static string GetVkRequestStringForCode()
        {
            const string reqStrTemplate = "http://api.vkontakte.ru/oauth/authorize?client_id={0}&scope=offline,wall";
            return string.Format(reqStrTemplate, VkConsts.ApplicationId);
        }

        public static void SetTrueCertificateValidation()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true; 
                };
        }

        public static string GetToken(string code)
        {
            var reqStrTemplate =
              "https://api.vkontakte.ru/oauth/access_token?client_id={0}&client_secret={1}&code={2}";

            var reqStr = string.Format(
                reqStrTemplate,
                VkConsts.ApplicationId,
                VkConsts.SecurityKey,
                code);

            Debug.Log("Here");
            Debug.Log("ReqString is: " + reqStr);
            var response = "";

            try
            {
                SetTrueCertificateValidation();
                var webClient = new WebClient();
                response = webClient.DownloadString(reqStr);
            }
            catch (Exception ex)
            {
                Debug.Log("Exception Web client: " + ex);
                throw ex;
            }

            Debug.Log(response);

            var cleanresponse = response.Replace('{', ' ').Replace('}', ' ');

            Debug.Log(cleanresponse);

            var values = cleanresponse.Split(',');

            foreach (var value in values)
            {
                Debug.Log(value);
                var keyVal = value.Split(':');

                if (DeleteQuots(keyVal[0]).Trim() == "access_token")
                {
                    Debug.Log("I find access code: " + DeleteQuots(keyVal[1]).Trim());
                    return DeleteQuots(keyVal[1]).Trim();
                }
            }

            throw new Exception("No key access_token");
        }

        public static void PostMessage(string token, string message)
        {
            var reqStr = string.Format(
              "https://api.vkontakte.ru/method/wall.post?access_token={0}&message={1}",
               token,
               message);
            SetTrueCertificateValidation();
            var webClient = new WebClient();
            webClient.DownloadString(reqStr);
        }

        private static string DeleteQuots(string data)
        {
            return data.Replace('\"', ' ');
        }
    }
}
