﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts
{
    public sealed class GameOverMenu : MonoBehaviour
    {
        public Vector2 ButtonExit;
        public Vector2 ButtonToVK;
        public Vector2 EnterCode;
        public Vector2 CodeField;
        public Vector2 ErrorText;
        public GUIStyle TextStyle;
        public Vector2 PostButton;

        private void Awake()
        {
            _isWaitToVkCode = false;
            _errorWithCode = "";
        }

        private void Update()
        {
        }

        private IEnumerator PasteFix()
        {
            yield return new WaitForSeconds (0.5f);
            if ((GUI.GetNameOfFocusedControl() == "keycode") && (_vkCode == ""))
            {
                _vkCode = Shell("pbpaste", "");
            }
        }

        private string Shell(string filename, string arguments)
        {
            var process = new Process();
            process.StartInfo.Arguments = arguments;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.FileName = filename;
            process.Start();
            var result = process.StandardOutput.ReadToEnd();

            process.WaitForExit();
            return result;
        }

        private void OnGUI()
        {
            if (_isWaitToVkCode)
            {
                if (!String.IsNullOrEmpty(_errorWithCode))
                {
                    GUI.Label(
                        Utils.CenterdRect(ErrorText, 70, 20), 
                        "Error with code: ", 
                        TextStyle);
                }

                GUI.Label(
                    Utils.CenterdRect(EnterCode, 70, 20), 
                    "Enter code from url parameter.", 
                    TextStyle);

                _vkCode = GUI.TextField(Utils.CenterdRect(CodeField, 200, 20), _vkCode);

                if (GUI.Button(
                        Utils.CenterdRect(PostButton, 200, 20), 
                        "This is my code!"))
                {
                    try
                    {
                        Utils.PostMessage(
                            Utils.GetToken(_vkCode), 
                            Utils.GetVkPostMessage());
                    }
                    catch (Exception e)
                    {
                        _errorWithCode = e.ToString();
                        return;
                    }

                    Application.LoadLevel(Consts.MenuLevelName);
                }
            }
            else
            { 
                if (GUI.Button(ButtonExitRect, "To main menu."))
                {
                    Debug.Log("You press Exit button!");
                    Network.Disconnect();
                    Application.LoadLevel(Consts.MenuLevelName);
                }

                if (GUI.Button(ButtonVkRect, "Share result to VK!"))
                {
                    Debug.Log("You press VK button!");
                    GetVkCode();
                }
            }
        }

        private bool _isWaitToVkCode;
        private string _errorWithCode = "";
        private string _vkCode = "";

        private void GetVkCode()
        {
            _isWaitToVkCode = true;
            Process.Start(Utils.GetVkRequestStringForCode());
        }

        private Rect ButtonExitRect
        {
            get
            {
                return new Rect(
                    Consts.ScreenCenter.x + ButtonExit.x,
                    Consts.ScreenCenter.y + ButtonExit.y,
                    140,
                    50);
            }
        }

        private Rect ButtonVkRect
        {
            get
            {
                return new Rect(
                    Consts.ScreenCenter.x + ButtonToVK.x, 
                    Consts.ScreenCenter.y + ButtonToVK.y, 
                    140, 
                    50);
            }
        }
    }
}