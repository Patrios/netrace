﻿using UnityEngine;

// ReSharper disable once MemberCanBePrivate.Global
// ReSharper disable once ConvertToConstant.Global
namespace Assets.Scripts
{
    public sealed class PlayerMovement : OnGameControlScript
    {
        public void Awake()
        {
            var mainCamera = transform.FindChild("Main Camera");
            var listener = mainCamera.GetComponent<AudioListener>();
            if (networkView.isMine)
            {
                transform.Find("PlayerName")
                    .GetComponent<TextMesh>()
                    .text = GameData.PlayerName;

                listener.enabled = true;
            }
            else
            {
                transform.Find("PlayerName")
                    .GetComponent<TextMesh>()
                    .text = GameData.OpponentName;

                listener.enabled = false;
                mainCamera.GetComponent<Camera>().enabled = false;
            }

            networkView.observed = this;
            networkView.stateSynchronization = NetworkStateSynchronization.Unreliable;
            _lastSyncTime = Time.time;
        }

        // Update is called once per frame
        protected override void HandleControl()
        {
            if (networkView.isMine)
            {
                if (IsMoveForward)
                {
                    RecalculateForceForward();
                }

                if (IsMoveBack)
                {
                    RecalculateForceBackward();
                }

                rigidbody.angularDrag = Mathf.Lerp(currentAngularDrag, maxAngularDrag, CurrentSpeed*0.1f);
                rigidbody.drag = Mathf.Lerp(currentDrag, maxDrag, CurrentSpeed*0.1f);
            }
            else
            {
                _syncTime += Time.fixedDeltaTime;
                rigidbody.position = Vector3.Lerp(_syncStartPosition, _syncEndPosition, _syncTime / _syncDelay);
                rigidbody.rotation = Quaternion.Lerp(_syncStartRotation, _syncEndRotation, _syncTime / _syncDelay); 
            }
        }

        void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
        {
            var syncPosition = Vector3.zero;
            var syncVelocity = Vector3.zero;
            var syncRotation = Quaternion.identity;

            if (stream.isWriting)
            {
                syncPosition = rigidbody.position;
                stream.Serialize(ref syncPosition);

                syncPosition = rigidbody.velocity;
                stream.Serialize(ref syncVelocity);

                syncRotation = rigidbody.rotation;
                stream.Serialize(ref syncRotation);
            }
            else
            {
                stream.Serialize(ref syncPosition);

                stream.Serialize(ref syncVelocity);
                stream.Serialize(ref syncRotation);

                _syncTime = 0f;
                _syncDelay = Time.time - _lastSyncTime;
                _lastSyncTime = Time.time;

                _syncEndPosition = syncPosition + syncVelocity * _syncDelay; 
                _syncStartPosition = rigidbody.position;

                _syncEndRotation = syncRotation;
                _syncStartRotation = rigidbody.rotation;
            }
        }
    
        #region SyncData

        private float _syncDelay = 0f;
        private float _syncTime = 0f;
        private float _lastSyncTime;

        private Vector3 _syncStartPosition = Vector3.zero; 
        private Vector3 _syncEndPosition = Vector3.zero;

        private Quaternion _syncStartRotation = Quaternion.identity;
        private Quaternion _syncEndRotation = Quaternion.identity;

        #endregion

        public float ForwardSpeed  = 10.0f;
        public float ReverseSpeed  = 5.0f;
        public float TurnSpeed     = 0.6f;

        public float MinTurnSpeed = 0.5f;

        public float CurrentSpeed
        {
            get { return Mathf.Abs(transform.InverseTransformDirection(rigidbody.velocity).y); }
        }

        private void RecalculateForceForward()
        {
            RecalculateForce(ForwardSpeed, -TurnSpeed);
        }

        private void RecalculateForceBackward()
        {
            RecalculateForce(ReverseSpeed, TurnSpeed);
        }

        private void RecalculateForce(float moveSpeed, float turnSpeed)
        {
            _moveDirection = moveSpeed * Input.GetAxis("Vertical");

            rigidbody.AddRelativeForce(0, _moveDirection, 0);

            if (CurrentSpeed > MinTurnSpeed)
            {
                _turnDirection = Input.GetAxis("Horizontal") * turnSpeed;
                rigidbody.AddRelativeTorque(0, 0, _turnDirection);
            }
        }

        private float _moveDirection = 0.0f;
        private float _turnDirection = 0.0f;

        private float maxAngularDrag = 2.5f;
        private float currentAngularDrag = 1.0f;
        private float maxDrag = 1.0f;
        private float currentDrag = 2.5f;

        private bool IsMoveForward
        {
            get { return Input.GetAxis("Vertical") > 0.0f; }
        }

        private bool IsMoveBack
        {
            get { return Input.GetAxis("Vertical") < 0.0f; }
        }

        private NetworkView _networkView;
    }
}
