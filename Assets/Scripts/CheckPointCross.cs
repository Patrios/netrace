﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class CheckPointCross : MonoBehaviour
    {
        private void Awake()
        {
            _isCrossed = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals(Consts.PlayerTag) && other.gameObject.networkView.isMine)
            {
                _isCrossed = true;
                Debug.Log("Cross Check Box!");
            }
        }

        public bool IsCrossed
        {
            get { return _isCrossed; }
        }

        private bool _isCrossed = false;
    }
}