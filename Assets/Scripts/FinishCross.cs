﻿using System.Linq;
using Assets.Scripts.Web;
using UnityEngine;

namespace Assets.Scripts
{
    public sealed class FinishCross : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals(Consts.PlayerTag) && other.gameObject.networkView.isMine)
            {
                var checkPoints = GameObject.FindGameObjectsWithTag(Consts.CheckpointTag);

                if (IsDebugMode || checkPoints.All(point => point.GetComponent<CheckPointCross>().IsCrossed))
                {
                    var playerName = GameData.PlayerName;
                    GameData.FinishTime = Utils.GetTimeInRace();

                    networkView.RPC("TryRegistrateWinner", RPCMode.All, playerName);

                    if (IsLast(playerName))
                    {
                        Utils.StopTimeManager();
                    }

                    Instantiate(WinMenuPrefab, Vector3.zero, Quaternion.identity);

                    Debug.Log("WinnerName: " + WinnerName + " PlayerName: " + playerName);

                    PrefabObjectsInitializer.InitializeGameOverMenu(
                        WinnerName == playerName, 
                        Utils.GetTotalScope());
                    
                    OnGameControlScript.GameInActiveState = false;

                    WebServiceUtils.GetUserService().SaveScope(
                        GameData.PlayerName,
                        Utils.GetTotalScope(),
                        Utils.IsCurrentPlayerWinner());

                    Debug.Log("Game Over!");
                }
            }
        }

        public bool IsDebugMode = true;

        public GameObject WinMenuPrefab;

        public string WinnerName = string.Empty;

        private bool IsLast(string playeName)
        {
            return playeName != WinnerName;
        }

        [RPC]
        private void TryRegistrateWinner(string playerName)
        {
            if (WinnerName == string.Empty)
            {
                Debug.Log("Winner name: " + playerName);
                WinnerName = playerName;
            }
        }
    }
}
