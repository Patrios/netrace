﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class OnGameControlScript : MonoBehaviour
    {
        static OnGameControlScript()
        {
            GameInActiveState = true;
        }

        public static bool GameInActiveState { get; set; }

        protected abstract void HandleControl();

        private void FixedUpdate()
        {
            if (!networkView.isMine || GameInActiveState)
            {
                HandleControl();
            }
        }
    }
}
