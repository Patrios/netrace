﻿using UnityEngine;

namespace Assets.Scripts
{
    public class NetworkObjectsInstantiator : MonoBehaviour
    {
        private void Awake()
        {
            
            if (Network.isServer)
            {
                Network.Instantiate(
                    PlayerPFB,
                    Consts.ServerStartPosition,
                    Quaternion.identity,
                    0);

                Network.Instantiate(CountDownManagerPFB, Vector3.zero, Quaternion.identity, 0);
                Network.Instantiate(TimeManagerPFB, Vector3.zero, Quaternion.identity, 0);
            }
            else
            {
                Network.Instantiate(
                    PlayerPFB,
                    Consts.ClientStartPosition,
                    Quaternion.identity,
                    0);
            }
        }

        public GameObject TimeManagerPFB;
        public GameObject CountDownManagerPFB;
        public GameObject PlayerPFB;
    }
}
