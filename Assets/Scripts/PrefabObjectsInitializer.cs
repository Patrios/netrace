﻿using System.Globalization;
using UnityEngine;

namespace Assets.Scripts
{
    public static class PrefabObjectsInitializer
    {
        public static void InitializeGameOverMenu(bool isWin, int scope)
        {
            var gameOverGuiText = GameObject.FindGameObjectWithTag(Consts.GameOverTextTag);
            gameOverGuiText.GetComponent<GUIText>().text = isWin ? Consts.OnWinText : Consts.OnLooseText;

            var scopeText = GameObject.FindGameObjectWithTag(Consts.ScopeTextTag);
            scopeText.GetComponent<GUIText>().text = scope.ToString(CultureInfo.InvariantCulture);
        }
    }
}
