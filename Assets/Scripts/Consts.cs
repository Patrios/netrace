﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class Consts
    {
        public const string PlayerTag = "Player";

        public const string CheckpointTag = "CheckPoint";

        public const string GameOverTextTag = "GameOverText";

        public const string ScopeTextTag = "ScopeText";

        public const string TimeManagerTag = "TimeManager";

        public static Vector2 ScreenCenter
        {
            get
            {
               return new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
            }
        }

        public const string OnWinText = "You Win!";
        public const string OnLooseText = "You Loose =(";

        public const int MaxScope = 100000;
        public const int WinnerScope = 1500;

        public const string RaceLevelName = "RaceLevel";
        public const string MenuLevelName = "MainMenuLevel";

        public const int ServerPort = 5300;
        public const int ServerConnections = 2;

        public static readonly Vector3 ServerStartPosition = new Vector3(9.0f, 12.0f, -0.1f);
        public static readonly Vector3 ClientStartPosition = new Vector3(7.0f, 12.0f, -0.1f);
    }
}
