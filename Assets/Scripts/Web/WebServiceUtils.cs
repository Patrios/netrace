﻿using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts.Web
{
    public static class WebServiceUtils
    {
        static WebServiceUtils()
        {
            UserService = new UserServiceClient(
                new BasicHttpBinding(),
                new EndpointAddress(GetWebServiceAddressFromResources()));
        }

        public static UserServiceClient GetUserService()
        {
            return UserService;
        }

        private static string GetWebServiceAddressFromResources()
        {
            var textFile = (TextAsset)Resources.Load("WebServiceAddress", typeof(TextAsset));
            Debug.Log(textFile == null);
            using (var textStream = new StringReader(textFile.text))
            {
                return textStream.ReadLine();
            }
        }

        private static readonly UserServiceClient UserService;
    }

}
