﻿namespace Assets.Scripts
{
    public class VkTokenResponse
    {
        // ReSharper disable InconsistentNaming
        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string user_id { get; set; }

        // ReSharper restore InconsistentNaming
    }
}
